cmake_minimum_required(VERSION 3.15)

project(tinyfiber)

include(cmake/CheckCompiler.cmake)
include(cmake/CompileOptions.cmake)
include(cmake/Sanitize.cmake)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "bin")

add_subdirectory(third_party)

include(cmake/ClangFormat.cmake)
include(cmake/ClangTidy.cmake)

set(LIB_TARGET ${PROJECT_NAME})

option(TINY_FIBER_DEVELOPER "TinyFiber developer mode" OFF)
option(TINY_FIBER_EXAMPLES "Enable TinyFiber examples" OFF)
option(TINY_FIBER_TESTS "Enable TinyFiber tests" OFF)
option(TINY_FIBER_BENCHMARKS "Enable TinyFiber benchmarks" OFF)

add_subdirectory(source)

enable_testing()

add_subdirectory(play)

if(TINY_FIBER_EXAMPLES OR TINY_FIBER_DEVELOPER)
    add_subdirectory(examples)
endif()

if(TINY_FIBER_TESTS OR TINY_FIBER_DEVELOPER)
    add_subdirectory(tests)
endif()
