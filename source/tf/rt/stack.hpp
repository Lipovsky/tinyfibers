#pragma once

#include <sure/stack/mmap.hpp>

namespace tf::rt {

using Stack = sure::stack::GuardedMmapStack;

}  // namespace tf::rt
