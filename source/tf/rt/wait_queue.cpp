#include <tf/rt/wait_queue.hpp>

#include <tf/rt/scheduler.hpp>

#include <wheels/core/assert.hpp>

namespace tf::rt {

void WaitQueue::Park() {
  auto* scheduler = Scheduler::Current();

  waiters_.PushBack(scheduler->RunningFiber());
  scheduler->Suspend();
}

void WaitQueue::WakeOne() {
  auto* scheduler = Scheduler::Current();

  if (waiters_.IsEmpty()) {
    return;
  }
  scheduler->Resume(waiters_.PopFront());
}

void WaitQueue::WakeAll() {
  auto* scheduler = Scheduler::Current();

  while (waiters_.NonEmpty()) {
    scheduler->Resume(waiters_.PopFront());
  }
}

WaitQueue::~WaitQueue() {
  WHEELS_ASSERT(waiters_.IsEmpty(), "WaitQueue is not empty");
}

}  // namespace tf::rt
