#include <tf/rt/fiber.hpp>

#include <tf/rt/scheduler.hpp>

#include <wheels/core/compiler.hpp>
#include <wheels/core/panic.hpp>
#include <wheels/core/exception.hpp>

namespace tf::rt {

Fiber::Fiber(Scheduler* sch, FiberRoutine r, Stack s, FiberId i)
    : scheduler(sch),
      routine(std::move(r)),
      stack(std::move(s)),
      id(i) {

  context.Setup(
      /*stack=*/stack.MutView(),
      /*trampoline=*/this);
}

Fiber::~Fiber() {
  if (watcher != nullptr) {
    watcher->OnCompleted();
  }
}

void Fiber::Run() noexcept {
  // Fiber execution starts here

  try {
    routine();
  } catch (...) {
    WHEELS_PANIC(
        "Uncaught exception in fiber: " << wheels::CurrentExceptionMessage());
  }

  scheduler->Terminate();  // Never returns

  WHEELS_UNREACHABLE();
}

}  // namespace tf::rt
