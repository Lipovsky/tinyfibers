# <sub>Tiny</sub>Fiber 🤖

Игрушечные файберы, написанные для образовательных целей.

## Цель

Продемонстрировать базовую механику работы потоков.

## Файберы

Файберы (_fibers_) – легковесные потоки, реализованные целиком в пространстве пользователя.

Файберы – _кооперативные_, они отдают управление планировщику только добровольно (например, при обращении к примитивам синхронизации или с помощью `Yield`).

Файберы исполняются в одном потоке операционной системы (_carrier thread_), который служит для них виртуальным процессором.

Однопоточный планировщик + кооперативность дают детерминизм исполнения.

## Пример

```cpp
#include <tf/run.hpp>
#include <tf/sched/yield.hpp>
#include <tf/sched/spawn.hpp>

#include <fmt/core.h>

int main() {
  // Разворачиваем планировщик и запускаем в нем первый файбер,
  // который будет исполнять переданную лямбду
  tf::RunScheduler([] {
    fmt::print("1");

    // Создаем и планируем новый файбер
    // Управление при этом остается у текущего файбера
    tf::JoinHandle h = tf::Spawn([] {
      fmt::print("->3");
      tf::Yield();  // Возвращаем управление родителю
      fmt::print("->5");
    });

    fmt::print("->2");
    tf::Yield();  // Передаем управление созданному выше файберу
    fmt::print("->4");

    h.Join();  // Останавливаем текущий файбер до завершения дочернего

    fmt::print("->6");
  });
  // Вызов RunScheduler завершится когда не останется готовых исполняться файберов

  return 0;
}
```

Вывод:
```
1->2->3->4->5->6
```

См. [примеры](/examples) и [тесты](/tests/fibers.cpp).

## Документация

- [User Guide](/docs/user-guide.md)
- [Диаграммы](https://disk.yandex.ru/d/QCh7Yk-FwSIjtA)

## Ограничения 

- Библиотека однопоточная
- Нет сети, каналов и т.п.

## References

### Context switch

- [System V ABI / AMD64](https://www.uclibc.org/docs/psABI-x86_64.pdf)
- [Context switch в Xv6](https://github.com/mit-pdos/xv6-public/blob/master/swtch.S)

### Fibers

- [Boost.fiber](https://github.com/boostorg/fiber)
- [folly::fibers](https://github.com/facebook/folly/blob/master/folly/fibers/README.md)
- [Marl](https://github.com/google/marl/)  
- [Project Loom: Fibers and Continuations for the Java Virtual Machine](https://cr.openjdk.java.net/~rpressler/loom/Loom-Proposal.html)
- [Fibers, Oh My!](https://graphitemaster.github.io/fibers/)

### Scheduler

- [xv6](https://github.com/mit-pdos/xv6-public/blob/eeb7b415dbcb12cc362d0783e41c3d1f44066b17/proc.c#L323)
- [Linux](https://github.com/torvalds/linux/blob/291009f656e8eaebbdfd3a8d99f6b190a9ce9deb/kernel/sched/core.c#L4921)
- [Golang](https://golang.org/src/runtime/proc.go)

## Опции CMake

- `TINY_FIBER_TESTS=ON` – тесты
- `TINY_FIBER_EXAMPLES=ON` – примеры

## Сборка

```shell
# Clone repo
git clone https://gitlab.com/Lipovsky/tinyfiber.git
cd tinyfiber
# Generate build files
mkdir build && cd build
cmake -DTINY_FIBER_EXAMPLES=ON ..
# Build `hello` example
make tinyfiber_example_hello
# Run example
./examples/hello/bin/tinyfiber_example_hello
```

## Зависимости

- [Wheels](https://gitlab.com/Lipovsky/wheels) – общие компоненты
- [Sure](https://gitlab.com/Lipovsky/sure) – контекст исполнения

### Внешние

- [Fmt](https://github.com/fmtlib/fmt) – форматированный вывод
